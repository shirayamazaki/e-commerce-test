import { useEffect, useState } from 'react';
import { Avatar, Col, List, Row, Typography } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { stringify } from 'query-string';
import Animate from 'rc-animate';
import Link from 'next/link';
import styles from './PurchaseHistory.module.scss';

const PurchaseHistoryApp = () => {
  const { Text } = Typography;
  const [PurchasedItem, setPurchasedItem] = useState([]);

  useEffect(() => {
    const data = localStorage.getItem("purchased") ? JSON.parse(localStorage.getItem("purchased")) : []
    setPurchasedItem(data)
  }, []);

  return (
    <Animate transitionName="fade" transitionAppear>
      <Row key={1} justify="space-around">
        <Col className={styles.modulContainer}>
          <Row className={styles.content} justify="space-between" align="middle">
            <Col style={{ width: '100%' }}>
              <Row className={styles.title} justify="space-between" align="middle">
                <Col span={2} gutter={1}>
                  <Row justify="space-around">
                    <Link href="/">
                      <ArrowLeftOutlined />
                    </Link>
                  </Row>
                </Col>
                <Col span={21}>
                  <Text strong>Purchase History</Text>
                </Col>
              </Row>
              <Row>
                <List
                  itemLayout="horizontal"
                  dataSource={PurchasedItem}
                  renderItem={item => (
                    <List.Item>
                      <Link href={{ pathname: '/ProductDetail', query: stringify(item) }}>
                        <List.Item.Meta
                          avatar={<Avatar src={item.imageUrl} size="large" />}
                          title={<a href="https://ant.design">{item.title}</a>}
                          description={
                            <div className={styles.customAntListDescription}>
                              {item.description}
                            </div>
                          }
                        />
                      </Link>
                    </List.Item>
                  )}
                />
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Animate>
  )
}

export default PurchaseHistoryApp;