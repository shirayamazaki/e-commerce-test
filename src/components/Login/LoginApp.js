import React from 'react';
import { Button, Checkbox, Col, Divider, Input, Row, Typography } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone, FacebookOutlined, GoogleOutlined } from '@ant-design/icons';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import styles from './LoginApp.module.scss';

const LoginApp = ({ onSubmit, onLoginFacebook, onLoginGoogle, onChangeEmail, onChangePassword }) => {
  const { Title } = Typography;
  return (
    <Row className={styles.loginContainer} justify="space-around" align="middle">
      <form onSubmit={onSubmit}>
        <Row justify="space-around">
          <Title>Login</Title>
        </Row>
        <Row className={styles.inputRow}>
          <Input 
            placeholder="Email" 
            className={styles.customInput}
            onChange={onChangeEmail}
          />
        </Row>
        <Row className={styles.inputRow}>
          <Input.Password 
            placeholder="Password"
            className={styles.customInput}
            iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
            onChange={onChangePassword}
          />
        </Row>
        <Row className={styles.inputRow} justify="space-between">
          <Col>
            <Checkbox>Remember me</Checkbox>
          </Col>
          <Col>
            <Button
              type="primary"
              htmlType="submit"
              shape="round"
            >
              Login
            </Button>
          </Col>
        </Row>
        <Divider>OR</Divider>
        <Row className={styles.socialLogin} justify="space-around">
          <FacebookLogin
            style={{ width: '100%' }}
            appId="1057020034803023"
            fields="name,email,picture"
            cssClass={styles.facebookLoginBtn}
            size="small"
            textButton="Login with Facebook"
            callback={onLoginFacebook}
            icon={<FacebookOutlined style={{ paddingRight: 10 }} />}
            version="2.3"
          />
        </Row>
        <Row className={styles.inputRow} justify="space-around">
          <GoogleLogin
            clientId="354855239173-fn5ee63fo1k7hstqvjhmrf1fdk6nc9uq.apps.googleusercontent.com"
            render={renderProps => (
              <Button 
                className={styles.googleLoginButton}
                type="primary" 
                htmlType="submit"
                shape="round"
                icon={<GoogleOutlined />}
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
              >
                Login with Google
              </Button>
            )}
            buttonText="Login"
            onSuccess={onLoginGoogle}
            onFailure={onLoginGoogle}
            cookiePolicy={'single_host_origin'}
          />
        </Row>
      </form>
    </Row>
  )
}

export default LoginApp;