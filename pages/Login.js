import { useRouter } from 'next/router';
import { useState } from 'react';
import Layout from '../src/components/Layout/Layout';
import LoginApp from '../src/components/Login/LoginApp';

const Login = () => {
  const [Email, setEmail] = useState("");
  const [Password, setPassword] = useState("");
  const router = useRouter();
  const onSubmit = (e) => {
    e.preventDefault()
    const params = {
      loginWith: "Email",
      response: {
        Email, Password
      }
    }
    localStorage.setItem("user", JSON.stringify(params));
    router.push("/");
  }
  
  const onLoginFacebook = (response) => {
    if (response !== null) {
      const params = {
        loginWith: "Facebook",
        response
      }
      localStorage.setItem("user", JSON.stringify(params));
      router.push("/");
    }
  }

  const onLoginGoogle = (response) => {
    if (response !== null) {
      const params = {
        loginWith: "Google",
        response
      }
      localStorage.setItem("user", JSON.stringify(params));
      router.push("/");
    }
  }

  const onChangeEmail = (e) => {
    setEmail(e.target.value)
  }

  const onChangePassword = (e) => {
    setPassword(e.target.value)
  }

  return (
    <Layout title="Login" description="E-Commerce Login">
      <LoginApp
        onChangeEmail={onChangeEmail}
        onChangePassword={onChangePassword}
        onSubmit={onSubmit}
        onLoginFacebook={onLoginFacebook}
        onLoginGoogle={onLoginGoogle}
      />
    </Layout>
  )
}

export default Login;