import { FINISH_GET_DATA, START_GET_DATA } from './home-action-type';
import { api } from '../../../../pages/api/apiary';

export const getHomeData = () => dispatch => {
  dispatch(startGetHomeData());
  api.get("/home")
  .then(res => {
    dispatch(finishGetHomeData(res.data));
  })
  .catch(err => {
    console.log(err);
    dispatch(finishGetHomeData([]))
  })
}

const startGetHomeData = () => ({
  type: START_GET_DATA
})

const finishGetHomeData = (payload) => ({
  type: FINISH_GET_DATA,
  payload
})