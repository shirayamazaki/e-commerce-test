import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Button, Col, Input, Modal, Row, Space, Typography } from 'antd';
import { ArrowLeftOutlined, FacebookFilled, HeartOutlined, HeartFilled, ShareAltOutlined, TwitterSquareFilled } from '@ant-design/icons';
import { FacebookShareButton, TwitterShareButton } from 'react-share';
import Link from 'next/link';
import Animate from 'rc-animate';
import styles from './ProductDetail.module.scss';

const ProductDetailApp = ({ productDetail, onClickBuy }) => {
  const router = useRouter();
  const { Text, Title } = Typography;
  const [LoveStatus, setLoveStatus] = useState(0);
  const [ModalShareVisibility, setModalShareVisibility] = useState(false);
  
  useEffect(() => {
    setLoveStatus(productDetail.loved)
  }, [productDetail])

  useEffect(() => {
    if (LoveStatus === "1") {
      const checkStorage = JSON.parse(localStorage.getItem("Wishlist"));
      if (!checkStorage) {
        localStorage.setItem("Wishlist", JSON.stringify([productDetail]));
      } else {
        checkStorage.unshift(productDetail);
        localStorage.setItem("Wishlist", JSON.stringify(checkStorage));
      }
    }
  }, [LoveStatus])

  const onClickLove = () => {
    LoveStatus === "0" ? setLoveStatus("1") : setLoveStatus("0");
  }

  const renderModalShare = () => {
    return (
      <Modal 
        title="Share" 
        visible={ModalShareVisibility} 
        onOk={() => setModalShareVisibility(false)} 
        onCancel={() => setModalShareVisibility(false)}
        centered
        footer={false}
      >
        <Row justify="space-around">
          <Space>
            <FacebookShareButton
              url={window.location.href}
              quote={`E-Commerce - ${productDetail.title}`}
              hashtag="#ECommerce"
            >
              <FacebookFilled style={{ fontSize: 32 }} />
            </FacebookShareButton>
            <TwitterShareButton
              url={window.location.href}
              quote={`E-Commerce - ${productDetail.title}`}
              hashtag="#ECommerce"
            >
              <TwitterSquareFilled style={{ fontSize: 32 }} />
            </TwitterShareButton>
          </Space>
        </Row>
        <Row style={{ marginTop: 20 }}>
          <Input value={window.location.href} />
        </Row>
      </Modal>
    )
  }

  return (
    <Animate transitionName="fade" transitionAppear>
      <Row key={1} justify="space-around">
        <Col className={styles.modulContainer}>
          <Row className={styles.content} justify="space-between" align="middle">
            <Col>
              <Row className={styles.title} justify="space-between" align="middle">
                <Col span={2} gutter={1}>
                  <Row justify="space-around">
                    <Link href="/">
                      <ArrowLeftOutlined />
                    </Link>
                  </Row>
                </Col>
                <Col span={21}>
                  <Text strong>{productDetail.title}</Text>
                </Col>
              </Row>
              <Row>
                <img src={productDetail.imageUrl} style={{ width: '100%' }} />
              </Row>
              <Row justify="space-between">
                <Col>
                  <Title strong>{productDetail.price}</Title>
                </Col>
                <Col>
                  <span className={styles.love} onClick={() => setModalShareVisibility(true)}><ShareAltOutlined /></span>
                  &nbsp;&nbsp;&nbsp;
                  <span className={styles.love} onClick={() => onClickLove(!LoveStatus)}>{ LoveStatus === "0" ? <HeartOutlined /> : <HeartFilled style={{ color: 'red' }} /> }</span>
                </Col>
              </Row>
              <Row style={{ marginTop: 20 }}>
                { productDetail.description }
              </Row>
              <Row justify="end" className={styles.menu}>
                <Button className={styles.btnGreen} type="primary" shape="round" onClick={onClickBuy}>Buy</Button>
              </Row>
            </Col>
          </Row>
        </Col>
        { renderModalShare() }
      </Row>
    </Animate>
  )
}

export default ProductDetailApp;