import { FINISH_GET_DATA, START_GET_DATA } from "./home-action-type"

const initialState = {
  homeData: [],
  isLoading: false
}

const handler = (currentState) => {
  const startGetHomeData = () => {
    return {
      ...currentState,
      isLoading: true
    }
  }

  const finishGetHomeData = (datas) => {
    return {
      ...currentState,
      isLoading: false,
      homeData: datas
    }
  }

  return {
    startGetHomeData,
    finishGetHomeData
  }
}

export default (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case START_GET_DATA:
      return handler(state).startGetHomeData();
    case FINISH_GET_DATA:
      return handler(state).finishGetHomeData(payload);
    default:
      return state;
  }
}