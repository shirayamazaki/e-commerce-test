import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Layout from "../src/components/Layout/Layout"
import ProductDetailApp from "../src/components/ProductDetail/ProductDetailApp"

const ProductDetail = () => {
  const router = useRouter();
  const [ProductDetail, setProductDetail] = useState({})

  useEffect(() => {
    setProductDetail(router.query);
  }, []);

  useEffect(() => {
    if (ProductDetail === undefined) {
      router.push("/")
    }
  }, [ProductDetail]);

  const onClickBuy = () => {
    const checkStorage = JSON.parse(localStorage.getItem("purchased"));
    if (!checkStorage) {
      localStorage.setItem("purchased", JSON.stringify([ProductDetail]));
    } else {
      checkStorage.unshift(ProductDetail);
      localStorage.setItem("purchased", JSON.stringify(checkStorage));
    }
  }

  return (
    <Layout title={ProductDetail.title ? ProductDetail.title : "Product Detail"} description={`Beli ${ProductDetail.title}`}>
      <ProductDetailApp productDetail={ProductDetail} onClickBuy={onClickBuy} />
    </Layout>
  )
}

export default ProductDetail;