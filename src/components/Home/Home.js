import { useEffect, useRef, useState } from 'react';
import { Button, Col, Input, Row, Typography } from 'antd';
import { HeartOutlined, SearchOutlined } from '@ant-design/icons';
import Link from 'next/link';
import Animate from 'rc-animate';
import styles from './Home.module.scss';
import CategoryCarousel from './CategoryCarousel';
import ItemCard from './ItemCard';
import SearchDrawer from './SearchDrawer';
import Menu from '../Gereral/Menu';

const Home = ({ ProfileData, isLoading, homeData }) => {
  const { Text } = Typography;
  const searchRef = useRef(null);
  const [ProductPromo, setProductPromo] = useState([]);
  const [Category, setCategory] = useState([]);
  const [searchDrawerVisibility, setSearchDrawerVisibility] = useState(false);
  const [SearchProductQuery, setSearchProductQuery] = useState("");

  useEffect(() => {
    setProductPromo((((homeData[0] || []).data || {}).productPromo || []));
    setCategory((((homeData[0] || []).data || {}).category || []));
  }, [homeData]);

  const onChangeQuerySearch = (e) => {
    setSearchProductQuery(e.target.value)
  }

  const renderContent = () => {
    return (
      <Col>
        <Row className={styles.carouselContainer}>
          <CategoryCarousel data={Category} />
        </Row>
        <Row justify="space-around" style={{ marginTop: 30 }}>
          <ItemCard data={ProductPromo} />
        </Row>
      </Col>
    )
  }

  return (
    <Animate transitionName="fade" transitionAppear>
      <Row key={1} justify="space-around">
        <Col className={styles.modulContainer}>
          <Row className={styles.content} justify="space-between" align="middle">
            <Col span={2}>
              <Row justify="space-around">
                <Link href="/Wishlist">
                  <HeartOutlined style={{ fontSize: 22 }} />
                </Link>
              </Row>
            </Col>
            <Col span={16} gutter={1}>
              <Input
                prefix={<SearchOutlined />}
                placeholder="Search"
                className={styles.customInput}
                onClick={() => { 
                  setSearchDrawerVisibility(true); 
                  setTimeout(() => {
                    searchRef.current.focus(); 
                  }, 100);
                }}
              />
            </Col>
            <Col span={5}>
              <Row justify="space-around">
                {
                  ProfileData ? <Button style={{ boxShadow: 'red 0px 1px 2px -1px', color: 'red' }} shape="round" onClick={() => localStorage.clear()}>Log out</Button> :
                  <Link href="/Login">
                    <Button style={{ boxShadow: 'rgb(135, 224, 0) 0px 1px 2px -1px', color: 'rgb(135, 224, 0)' }} shape="round">Login</Button>
                  </Link>
                }
              </Row>
            </Col>
          </Row>
          <Row className={styles.content}>
            { isLoading ? <Col>Loading ...</Col> : renderContent() }
          </Row>
          <Menu />
        </Col>
        <SearchDrawer 
          searchDrawerVisibility={searchDrawerVisibility} 
          setSearchDrawerVisibility={setSearchDrawerVisibility} 
          setSearchProductQuery={setSearchProductQuery}
          productQuery={SearchProductQuery}
          productPromo={ProductPromo} 
          inputComponent={
            <Input
              ref={searchRef}
              prefix={<SearchOutlined />}
              placeholder="Search"
              className={styles.customInput}
              onChange={onChangeQuerySearch}
              value={SearchProductQuery}
            />
          }
        />
      </Row>
    </Animate>
  )
}

export default Home;