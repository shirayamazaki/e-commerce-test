import { useEffect, useState } from 'react';
import Layout from '../src/components/Layout/Layout';
import WishlistApp from '../src/components/Wishlist/WishlistApp';
import 'antd/dist/antd.css';

const Wishlist = () => {
  const [Data, setData] = useState([]);

  useEffect(() => {
    localStorage.getItem("Wishlist") ? setData(JSON.parse(localStorage.getItem("Wishlist"))) : setData([]);
  }, [])

  return (
    <Layout title="Wishlist" description="E-Commerce Wishlist">
      <WishlistApp data={Data} />
    </Layout>
  )
}
export default Wishlist;