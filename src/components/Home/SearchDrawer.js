import { Avatar, Col, Drawer, List, Row } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { stringify } from 'query-string';
import styles from './Home.module.scss';
import Link from 'next/link';

const SearchDrawer = ({ searchDrawerVisibility, setSearchDrawerVisibility, productPromo, inputComponent, productQuery, setSearchProductQuery }) => {
  const listProduct = () => {
    let query = productQuery.toString();
    const list = query === "" ? [] : productPromo.filter(dt => dt.title.toLowerCase().includes(query.toLowerCase())).map(item => {
      item = {
        ...item
      };
      return item;
    })
    return list
  }

  return (
    <Drawer
        title={
          <Row justify="space-between" align="middle">
            <Col span={1} gutter={1}>
              <Row justify="space-around">
                <ArrowLeftOutlined onClick={() => { setSearchDrawerVisibility(false); setSearchProductQuery(""); }} />
              </Row>
            </Col>
            <Col span={22}>
              { inputComponent }
            </Col>
          </Row>
        }
        placement="bottom"
        closable={false}
        onClose={() => { setSearchDrawerVisibility(false); setSearchProductQuery(""); }}
        visible={searchDrawerVisibility}
        height="100%"
        bodyStyle={{ padding: '0 20px' }}
      >
        <List
          itemLayout="horizontal"
          dataSource={listProduct()}
          renderItem={item => (
            <List.Item>
              <Link href={{ pathname: '/ProductDetail', query: stringify(item) }}>
                <List.Item.Meta
                  avatar={<Avatar src={item.imageUrl} size="large" />}
                  title={<a href="https://ant.design">{item.title}</a>}
                  description={
                    <div className={styles.customAntListDescription}>
                      {item.description}
                    </div>
                  }
                />
              </Link>
            </List.Item>
          )}
        />
      </Drawer>
  )
}

export default SearchDrawer;