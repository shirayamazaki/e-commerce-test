import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getHomeData } from "../src/components/Home/store/home-action";
import Home from "../src/components/Home/Home"
import Layout from "../src/components/Layout/Layout";

const Dashboard = ({ getHomeData, isLoading, homeData }) => {
  const [Profile, setProfile] = useState({});
  useEffect(() => {
    getHomeData();
    setProfile(JSON.parse(localStorage.getItem("user")));
  }, []);
  
  useEffect(() => {
    setProfile(JSON.parse(localStorage.getItem("user")));
  }, [Profile])

  return (
    <Layout title="E-Commerce" description="E-Commerce Home">
      <Home ProfileData={Profile} isLoading={isLoading} homeData={homeData} />
    </Layout>
  )
}

const mapStateToProps = (state) => ({
  ...state.homeReducer
});

const mapDispatchToProps = (dispatch) => ({ 
    getHomeData: () => dispatch(getHomeData())
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);