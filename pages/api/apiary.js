import axios from 'axios';

export let api = axios.create({
  baseURL: "https://private-4639ce-ecommerce56.apiary-mock.com/",
  headers: {
    'Access-Control-Allow-Origin': "http://localhost:3000/",
    'Access-Control-Allow-Headers': "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
    'Content-Type': 'application/json',
    'Accept': '*/*'
  }
});