import { Col, Row } from 'antd';
import { HomeOutlined, LayoutOutlined, ShoppingCartOutlined, UserOutlined } from '@ant-design/icons';
import styles from '../Home/Home.module.scss';
import Link from 'next/link';

const Menu = () => {
  return (
    <Row justify="space-between" className={styles.menu}>
      <Col span={6} className={styles.menuItem}><HomeOutlined style={{ fontSize: 24 }} /></Col>
      <Col span={6} className={styles.menuItem}><LayoutOutlined style={{ fontSize: 24 }} /></Col>
      <Col span={6} className={styles.menuItem}><Link href="/PurchaseHistory"><ShoppingCartOutlined style={{ fontSize: 24 }} /></Link></Col>
      <Col span={6} className={styles.menuItem}><Link href="/Wishlist"><UserOutlined style={{ fontSize: 24 }} /></Link></Col>
    </Row>
  )
}

export default Menu;