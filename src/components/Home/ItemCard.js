import { Col, Row, Typography } from 'antd';
import { HeartOutlined, HeartFilled } from '@ant-design/icons';
import styles from './Home.module.scss';
import Link from 'next/link';
import { stringify } from 'query-string';

const ItemCard = ({ data }) => {
  const { Text } = Typography;
  
  return (
    <Col style={{ width: '100%' }}>
      {
        data.map((item, idx) => {
          return (
            <Link href={{ pathname: '/ProductDetail', query: stringify(item) }}>
              <Row key={item.id} justify="space-around" align="middle" className={styles.itemCardContainer} style={ data.length === idx+1 ? { marginBottom: 80 } : {} }>
                <Col>
                  <Row>
                    <img src={item.imageUrl} className={styles.itemCardImage} />
                    <span className={styles.love}>{ item.loved === 0 ? <HeartOutlined /> : <HeartFilled style={{ color: 'red' }} /> }</span>
                  </Row>
                  <Row className={styles.itemCardText}>
                    <Col>
                      <Row>
                        <Text>{item.title}</Text>
                      </Row>
                      <Row>
                        <Text strong>{item.price}</Text>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Link>
          )
        })
      }
    </Col>
  )
}

export default ItemCard;