import { Col, Row, Typography } from 'antd';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styles from './Home.module.scss';

const CategoryCarousel = ({ data }) => {
  const { Text } = Typography;
  const settings = {
    arrows: false,
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    swipe: true
  };
  
  return (
    <Slider {...settings}>
      {
        data.map(item => {
          return (
            <div key={item.id} style={{ padding: '0px 10px' }}>
              <Row justify="space-around" align="middle" className={styles.carouselItem}>
                <Col>
                  <Row justify="space-around">
                    <img src={item.imageUrl} className={styles.carouselItemImage} />
                  </Row>
                  <Row justify="space-around">
                    <Text className={styles.carouselItemText}>{item.name}</Text>
                  </Row>
                </Col>
              </Row>
            </div>
          )
        })
      }
    </Slider>
  )
}

export default CategoryCarousel;