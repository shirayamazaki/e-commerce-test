import Layout from '../src/components/Layout/Layout';
import PurchaseHistoryApp from '../src/components/PurchaseHistory/PurchaseHistoryApp';
import 'antd/dist/antd.css';

const PurchaseHistory = () => {
  return (
    <Layout title="Purchase History" description="E-Commerce Purchase History">
      <PurchaseHistoryApp />
    </Layout>
  )
}

export default PurchaseHistory;