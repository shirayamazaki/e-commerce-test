import Head from 'next/head';
import styles from './Layout.module.scss';

const Layout = ({ children, title, description }) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>{ title }</title>
        <link rel="icon" href="/favicon.ico" />
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content={description}></meta>
      </Head>

      <main className={styles.main}>
        {children}
      </main>
    </div>
  )
}

export default Layout;