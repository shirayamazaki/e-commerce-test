import Layout from '../src/components/Layout/Layout';
import Dashboard from './Home';
import 'antd/dist/antd.css';

const Index = () => {
  return (
    <Layout title="E-Commerce" description="E-Commerce Home">
      <Dashboard />
    </Layout>
  )
}

export default Index;